//
//  Drugs.swift
//  FirebaseCrud
//
//  Created by Mike on 4/15/19.
//  Copyright © 2019 codelapp. All rights reserved.
//

import Foundation
import UIKit
class Drugs {
    
    var name:String?
    var type:String?
    var id : String?
    var imagen : String?
    
    init(name:String?, type:String?, id: String?, imagen:String?) {
        self.name = name
        self.type = type
        self.id = id
        self.imagen = imagen
    }
    
}
