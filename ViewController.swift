import UIKit
import FirebaseAuth

class ViewController: UIViewController{
    @IBOutlet weak var control: UISegmentedControl!
    @IBOutlet weak var correo: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        login()
    }

    @IBAction func entrar(_ sender: UIButton) {
        if control.selectedSegmentIndex == 0 {
            iniciarSesion(correo: correo.text!, password: password.text!)
        }else{
            registrarse(correo: correo.text!, password: password.text!)
        }
    }
    
    func iniciarSesion(correo:String, password:String){
        Auth.auth().signIn(withEmail: correo, password: password) { (user, error) in
            
            if user != nil {
                self.performSegue(withIdentifier: "inicio", sender: self)
            }else{
                if let error = error?.localizedDescription {
                    let alert = UIAlertController(title: "Error", message: "Mail or pass incorrect", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true)
                    print("error firebase de inciar sesion", error)
                }else{
                    print("error de codigo")
                }
            }
            
        }
    }
    
    func registrarse(correo:String, password:String){
        if password.count <= 6  {
        let alert = UIAlertController(title: "Error", message: "Please, enter a pass with more of 6 characters.", preferredStyle: .alert)
       alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
        }else{
        Auth.auth().createUser(withEmail: correo, password: password) { (user, error) in
            if user != nil {
                self.performSegue(withIdentifier: "inicio", sender: self)
            }else{
                if let error = error?.localizedDescription {
                    let alert = UIAlertController(title: "Error", message: "The email address is already in use by another account.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true)
                    print("error firebase de registro", error)
                }else{
                    print("error de codigo")
                }
            }
        }
      }
    }
    
    
    func login(){
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user == nil{
                print("no estamos logeados")
            }else{
                self.performSegue(withIdentifier: "inicio", sender: self)
            }
        }
    }
}

