import UIKit
import FirebaseDatabase
import FirebaseStorage

class EditarViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var type: UITextField!
    var ref: DatabaseReference!
    var editDrugs: Drugs!
    var plataforma : String!
    var id = ""
    var portada = ""
    var imagen = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        name.text = editDrugs.name
        type.text = editDrugs.type
        id = editDrugs.id!
        portada = editDrugs.imagen!
    }

    @IBAction func editar(_ sender: UIButton) {
        let storage = Storage.storage().reference()
        let nombreImagen = UUID()
        let directorio = storage.child("imagenes/\(nombreImagen)")
        let metaData = StorageMetadata()
        metaData.contentType = "image/png"
        directorio.putData(UIImagePNGRepresentation(imagen)!, metadata: metaData) { (data, error) in
            if error == nil {
                print("cargo la imagen")
                let borrarImagen = Storage.storage().reference(forURL: self.portada)
                borrarImagen.delete(completion: nil)
                
                let campos = ["name": self.name.text!,
                              "type": self.type.text!,
                              "id": self.id,
                              "portada": String(describing: directorio)]
                self.ref.child(self.plataforma).child(self.id).setValue(campos)
                self.dismiss(animated: true, completion: nil)
                
            }else{
                if let error = error?.localizedDescription {
                    print("error al subir imagen en firebase", error)
                }else{
                    print("error en el codigo")
                }
            }
        }
    }
    
   
    @IBAction func camara(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func cancelar(_ sender: UIButton) {
       dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let imagenTomada = info[UIImagePickerControllerEditedImage] as? UIImage
        imagen = imagenTomada!
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func deleteDrug(id:String){
        ref.child(id).setValue(nil)
    }
    
   
    @IBAction func buDelete(_ sender: UIButton) {
        print("entro delete")
        self.deleteDrug(id: editDrugs.id!)
        print("id" + editDrugs.id!)
        
    }
}
