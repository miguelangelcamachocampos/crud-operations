import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class InicioViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var cargador: UIActivityIndicatorView!
    @IBOutlet weak var droga: UITextField!
    @IBOutlet weak var type: UITextField!
    @IBOutlet weak var vistaPicker: UILabel!
    @IBOutlet weak var picker: UIPickerView!
    var drug:String = ""
    var imagen = UIImage()
    let drugs = ["ESTIMULANTES", "DEPRESORAS","ALUCINOGENAS"]
    var ref:DatabaseReference!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        let correo = Auth.auth().currentUser?.email
        print("El correo electronico del usuario es: \(correo!)")
        ref = Database.database().reference()
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return drugs[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return drugs.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        drug = drugs[row]
        vistaPicker.text = drugs[row]
    }
    
    @IBAction func guardar(_ sender: UIButton) {
        
        let id = ref.childByAutoId().key
        let storage = Storage.storage().reference()
        let nombreImagen = UUID()
        let directorio = storage.child("imagenes/\(nombreImagen)")
        let metaData = StorageMetadata()
        metaData.contentType = "image/png"
        directorio.putData(UIImagePNGRepresentation(imagen)!, metadata: metaData) { (data, error) in
            if error == nil {
                print("cargo la imagen")
                self.cargador.stopAnimating()
                self.cargador.isHidden = true
            }else{
                if let error = error?.localizedDescription {
                    print("error al subir imagen en firebase", error)
                }else{
                    print("error en el codigo")
                }
            }
        }
        
        
        let campos = ["name":  droga.text!,
                      "type":   type.text!,
                      "id":     id,
                      "portada":String(describing: directorio)]
        
        ref.child(drug).child(id).setValue(campos)
        cargador.isHidden = false
        cargador.startAnimating()
        print("guardo")
        limpiar()
        
    }
    
    @IBAction func salir(_ sender: UIButton) {
        try! Auth.auth().signOut()
        performSegue(withIdentifier: "login", sender: self)
    }
    func limpiar() {
        droga.text = ""
        type.text = ""
    }
        
    @IBAction func tomarFoto(_ sender: UIBarButtonItem) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let imagenTomada = info[UIImagePickerControllerEditedImage] as? UIImage
        imagen = imagenTomada!
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

}
