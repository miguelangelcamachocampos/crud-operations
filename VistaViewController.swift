import UIKit
import FirebaseDatabase
import FirebaseStorage

class VistaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tabla: UITableView!
    @IBOutlet weak var control: UISegmentedControl!
    var listDrugs = [Drugs]()
    var ref: DatabaseReference!
    var handle: DatabaseHandle!
    var consola = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabla.delegate = self
        tabla.dataSource = self
        ref = Database.database().reference()
        plataformas(plat: "ESTIMULANTES")
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDrugs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tabla.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Celda
        let drug : Drugs
        
        drug = listDrugs[indexPath.row]
        
        cell.name.text = drug.name
        cell.type.text = drug.type
    
        if let urlFoto = drug.imagen {
            Storage.storage().reference(forURL: urlFoto).getData(maxSize: 10 * 1024 * 1024, completion: { (data, error) in
                if let error = error?.localizedDescription {
                    print("fallo al traer imagenes", error)
                }else {
                    cell.imagenFirebase.image = UIImage(data: data!)
                    cell.imagenFirebase.layer.masksToBounds = false
                    cell.imagenFirebase.layer.cornerRadius = cell.imagenFirebase.frame.height/2
                    cell.imagenFirebase.clipsToBounds = true
                    cell.imagenFirebase.layer.borderWidth = 2
                    self.tabla.reloadData()
                }
            })
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "editar", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editar" {
            if let id = tabla.indexPathForSelectedRow {
                let fila = listDrugs[id.row]
                let destino = segue.destination as! EditarViewController
                destino.editDrugs = fila
                destino.plataforma = consola
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let borrar = UITableViewRowAction(style: .destructive, title: "Eliminar") { (action, indexPath) in
            let drug : Drugs
            drug = self.listDrugs[indexPath.row]
            let id = drug.id
            let url = drug.imagen
            self.ref.child(self.consola).child(id!).setValue(nil)
            
            let borrarImagen = Storage.storage().reference(forURL: url!)
            borrarImagen.delete(completion: nil)
        }
        return [borrar]
    }
    
    
    @IBAction func atras(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
   
    @IBAction func controlButton(_ sender: Any) {
        if control.selectedSegmentIndex == 0 {
            plataformas(plat: "ESTIMULANTES")
            consola = "ESTIMULANTES"
        } else if control.selectedSegmentIndex == 1 {
            plataformas(plat: "DEPRESORAS")
            consola = "DEPRESORAS"
        } else {
            plataformas(plat: "ALUCINOGENAS")
            consola = "ALUCINOGENAS"
        }
        
    }
    
    func plataformas(plat: String){
        handle = ref.child(plat).observe(DataEventType.value, with: { (snapshot) in
            self.listDrugs.removeAll()
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                let valores = item.value as? [String:AnyObject]
                let name = valores!["name"] as? String
                let type = valores!["type"] as? String
                let id = valores!["id"] as? String
                let url = valores!["portada"] as? String
                let drugs = Drugs(name: name, type: type, id: id, imagen: url)
                self.listDrugs.append(drugs)
            }
            self.tabla.reloadData()
        })
        consola = "DEPRESORAS"
    }
    
    func deleteDrug(id:String){
        ref.child(id).setValue(nil)
    }
    
}
